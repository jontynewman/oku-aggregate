<?php

namespace JontyNewman\Oku\Tests;

use ArrayObject;
use GuzzleHttp\Psr7\ServerRequest;
use InvalidArgumentException;
use JontyNewman\Oku\Aggregate;
use JontyNewman\Oku\Aggregate\Input;
use JontyNewman\Oku\Context\Sessions\RequestHandlerSession;
use JontyNewman\Oku\ContextInterface;
use JontyNewman\Oku\Contexts\RequestHandlerContext;
use JontyNewman\Oku\Helpers\Html;
use JontyNewman\Oku\ResponseBuilderInterface;
use JontyNewman\Oku\ResponseBuilders\RequestHandlerResponseBuilder;
use OutOfBoundsException;
use PHPUnit\Framework\TestCase;
use ShrooPHP\Framework\Sessions\Session;

class AggregateTest extends TestCase
{
	public function test()
	{
		$this->assertAggregate();
	}

	public function testWithName()
	{
		$this->assertAggregate('name');
	}

	private function assertAggregate(string $name = null)
	{
		$exception = null;
		$invoked = false;
		$offset = '/';
		$firstValue = 'first';
		$secondValue = 'second';
		$aggregate = new Aggregate($name);
		$first = new ArrayObject();
		$second = new ArrayObject();
		$request = new ServerRequest('POST', $offset);
		$builder = new RequestHandlerResponseBuilder();
		$session = new RequestHandlerSession(new Session());
		$context = new RequestHandlerContext($request, $session, '', '', '', '');
		$callback = function (
			ResponseBuilderInterface $actual_builder,
			ContextInterface $actual_context
		) use (&$invoked, $builder, $context) {

			$this->assertSame($builder, $actual_builder);
			$this->assertSame($context, $actual_context);
			$invoked = true;
		};

		if (is_null($name)) {
			$name = Aggregate::NAME;
		}

		$aggregate->set($firstValue, $first);
		$aggregate->set($secondValue, $second);

		$this->assertInput(
			$aggregate->input($offset, $firstValue),
			$name,
			$firstValue
		);

		$this->assertInput(
			$aggregate->input($offset, $secondValue),
			$name,
			$secondValue
		);

		$this->assertFalse($aggregate->offsetExists($offset));

		try {
			$aggregate->offsetSet($offset, '');
		} catch (InvalidArgumentException $exception) {
			$this->assertSame(
				'Expected value to be a PSR-compliant server request',
				$exception->getMessage()
			);
		}

		$this->assertNotNull($exception);
		$exception = null;

		$aggregate->offsetSet($offset, $request);

		$this->assertFalse($first->offsetExists($offset));
		$this->assertFalse($second->offsetExists($offset));

		$withFirst = $request->withParsedBody([$name => $firstValue]);

		$aggregate->offsetSet($offset, $withFirst);

		$this->assertTrue($first->offsetExists($offset));
		$this->assertFalse($second->offsetExists($offset));

		$this->assertInput(
			$aggregate->input($offset, $firstValue),
			$name,
			$firstValue
		);

		$this->assertInput(
			$aggregate->input($offset, $secondValue),
			$name,
			$secondValue
		);

		$this->assertInput(
			$aggregate->input($offset, $firstValue, true),
			$name,
			$firstValue,
			true
		);

		$this->assertSame($withFirst, $first->offsetGet($offset));

		$withSecond = $request->withParsedBody([$name => $secondValue]);

		$aggregate->offsetSet($offset, $withSecond);

		$this->assertFalse($first->offsetExists($offset));
		$this->assertTrue($second->offsetExists($offset));

		$this->assertInput(
			$aggregate->input($offset, $firstValue),
			$name,
			$firstValue,
			true
		);

		$this->assertInput(
			$aggregate->input($offset, $firstValue, true),
			$name,
			$firstValue
		);

		$this->assertInput(
			$aggregate->input($offset, $secondValue),
			$name,
			$secondValue,
			true
		);

		$this->assertSame($withSecond, $second->offsetGet($offset));

		$second->offsetSet($offset, $callback);

		$callable = $aggregate->offsetGet($offset);

		$this->assertTrue(is_callable($callable));

		($callable)($builder, $context);

		$this->assertTrue($invoked);

		try {
			($aggregate->offsetGet(''))($builder, $context);
		} catch (OutOfBoundsException $exception) {
			$this->assertSame(
				"No repositories contain the offset ''",
				$exception->getMessage()
			);
		}

		$this->assertNotNull($exception);

		$aggregate->offsetUnset($offset);

		$this->assertFalse($first->offsetExists($offset));
		$this->assertFalse($second->offsetExists($offset));

		$this->assertInput(
			$aggregate->input($offset, $firstValue),
			$name,
			$firstValue
		);

		$this->assertInput(
			$aggregate->input($offset, $secondValue),
			$name,
			$secondValue,
			true
		);

		$this->assertInput(
			$aggregate->input($offset, $secondValue, true),
			$name,
			$secondValue
		);
	}

	private function assertInput(
		Input $input,
		string $name,
		string $value,
		bool $current = false
	) {

		$attributes = [
			'name' => $name,
			'value' => $value,
		];

		$this->assertSame($name, $input->name());
		$this->assertSame($value, $input->value());
		$this->assertSame($current, $input->checked());

		$this->assertSame(Html::tag('input', $attributes), $input->html());

		$attributes['type'] = 'radio';

		if ($current) {
			$attributes['checked'] = 'checked';
		}

		$this->assertSame(Html::tag('input', $attributes), (string) $input);
	}
}
