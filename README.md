# JontyNewman\Oku\Aggregate

Functionality for aggregating multiple repositories into a single repository for
[JontyNewman\Oku](https://gitlab.com/jontynewman/oku).

## Installation

```
composer require 'jontynewman/oku-aggregate ^1.1'
```
## Example

The following example assumes that
[`rst2html5`](http://docutils.sourceforge.net/docs/user/tools.html#rst2html5-py)
is available, and that the following packages are installed.

- [`jontynewman/html-filter`](https://gitlab.com/jontynewman/html-filter)
- [`jontynewman/oku-io`](https://gitlab.com/jontynewman/oku-io)
- [`jontynewman/oku-proc`](https://gitlab.com/jontynewman/oku-proc)
- [`jontynewman/oku-upload`](https://gitlab.com/jontynewman/oku-upload)

The editor will allow users to either input reStructuredText or upload files in
order to create static web pages.

```php
<?php

use GuzzleHttp\Psr7\Response;
use JontyNewman\HtmlFilter;
use JontyNewman\Oku\Aggregate;
use JontyNewman\Oku\ContextInterface;
use JontyNewman\Oku\IO\Cache;
use JontyNewman\Oku\IO\Repository as IoRepository;
use JontyNewman\Oku\Process;
use JontyNewman\Oku\RequestHandler;
use JontyNewman\Oku\ResponseBuilderInterface;
use JontyNewman\Oku\Upload\Repository as UploadRepository;

require 'vendor/autoload.php';

$dir = '/path/to/aggregate/directories';

// Convert reStructuredText input files to HTML output files.
$process = new Process('rst2html5');

// Cache HTML output in the specified directory.
$cache = new Cache($process, "{$dir}/html/", 'html');

// Construct an empty repository of repositories.
$aggregate = new Aggregate();

// Persist reStructuredText input in the specified directory (for future edits).
$repository = new IoRepository($cache, "{$dir}/rst/", 'rst');

// Associate reStructuredText with the aggregate value 'io'.
$aggregate->set('io', $repository);

// Persist uploaded files and associate them with the aggregate value 'upload'.
$aggregate->set('upload', new UploadRepository("{$dir}/uploads/"));

// Allow users to either edit reStructuredText or upload files.
$editor = function (
    ResponseBuilderInterface $builder,
    ContextInterface $context
) use ($aggregate, $repository): void {

    $path = $context->request()->getUri()->getPath();
    $in = $repository->directory()->offsetGet($path);
    $render = function () use ($in) {

        if (file_exists($in)) {
            HtmlFilter::passthru($in, 'rb');
        }
    };

    $builder->content(function () use ($path, $aggregate, $render, $context) {

        $io = $aggregate->input($path, 'io');
        $upload = $aggregate->input($path, 'upload');
        require 'editor.php';
    });
};

// Use a simple 404 page as the default response.
$default = new Response(404, ['Content-Type' => 'text/plain'], 'Not Found');

// Set up the request handler.
$handler = new RequestHandler($aggregate, $default, $editor);

// Run the application.
$handler->run();

```
An example template of the editor (i.e. `editor.php`) follows.

```php
<?php

use JontyNewman\Oku\Aggregate;
use JontyNewman\Oku\IO\Repository as IoRepository;
use JontyNewman\Oku\Upload\Repository as UploadRepository;
use JontyNewman\Oku\Helpers\Html;

/* @var $path string */
/* @var $aggregate \JontyNewman\Oku\Aggregate */
/* @var $io \JontyNewman\Oku\Aggregate\Input */
/* @var $upload \JontyNewman\Oku\Aggregate\Input */
/* @var $render callable */
/* @var $context \JontyNewman\Oku\ContextInterface */

?>
<!DOCTYPE html>
<html>
  <head>
    <title>JontyNewman\Oku\Aggregate</title>
  </head>
  <body>
    <form action="" method="post" enctype="multipart/form-data">
      <p>
        <label><?= $io; ?>reStructuredText</label>
      </p>
      <p>
        <label>
          Source
          <textarea name="<?= Html::escape(IoRepository::TEXT); ?>"><?php ($render)(); ?></textarea>
        </label>
      </p>
      <p>
        <label>
          Document
          <input type="file" name="<?= Html::escape(IoRepository::FILE); ?>">
        </label>
      </p>
      <p>
        <label><?= $upload; ?>Upload</label>
      </p>
      <p>
        <label>
          File
          <input type="file" name="<?= Html::escape(UploadRepository::NAME); ?>">
        </label>
      </p>
      <p>
        <?= $context->token(); ?>
        <?= $context->put(); ?>
        <input type="submit" value="Save">
      </p>
    </form>
    <form action="" method="post">
      <p>
        <?= $context->token(); ?>
        <?= $context->delete(); ?>
        <input type="submit" value="Delete">
      </p>
    </form>
    <?= $context->inset(); ?>
  </body>
</html>

```
