<?php

namespace JontyNewman\Oku\Aggregate;

use JontyNewman\Oku\Context\InputInterface;
use JontyNewman\Oku\Helpers\Html;

/**
 * A form input for distinguishing repositories.
 */
class Input implements InputInterface
{
	/**
	 * The name of the input.
	 *
	 * @var string
	 */
	private $name;

	/**
	 * The value of the input.
	 *
	 * @var string
	 */
	private $value;

	/**
	 * Whether or not the associated repository is the current repository.
	 *
	 * @var bool
	 */
	private $current;

	public function __construct(
		string $name,
		string $value,
		bool $current = false
	) {
		$this->name = $name;
		$this->value = $value;
		$this->current = $current;
	}

	public function name(): string
	{
		return $this->name;
	}

	public function value(): string
	{
		return $this->value;
	}

	/**
	 * Determines whether or not the rendered form input should be checked (with
	 * the assumption that it is either a checkbox or a radio button).
	 *
	 * @return bool Whether or not the rendered form input should be checked.
	 */
	public function checked(): bool
	{
		return $this->current;
	}

	public function html(
		array $attributes = [],
		int $flags = null,
		string $encoding = null
	): string {

		$immutable = [
			'name' => $this->name,
			'value' => $this->value,
		];

		return Html::tag('input', $immutable + $attributes, $flags, $encoding);
	}

	public function __toString(): string
	{
		$attributes = [
			'type' => 'radio',
		];

		if ($this->current) {
			$attributes['checked'] = 'checked';
		}

		return $this->html($attributes);
	}
}
