<?php

namespace JontyNewman\Oku;

use ArrayAccess;
use InvalidArgumentException;
use JontyNewman\Oku\Aggregate\Input;
use JontyNewman\Oku\ContextInterface;
use JontyNewman\Oku\ResponseBuilderInterface;
use OutOfBoundsException;
use Psr\Http\Message\ServerRequestInterface;

/**
 * An aggregation of repositories.
 */
class Aggregate implements ArrayAccess
{

	/**
	 * The default key.
	 */
	const NAME = 'aggregate';

	/**
	 * The key being used to identify the current repository.
	 *
	 * @var string $name
	 */
	private $name;

	/**
	 * The repositories that are forming the aggregate.
	 *
	 * @var array
	 */
	private $repositories;

	/**
	 * The last known current repository values (if any).
	 *
	 * @var array
	 */
	private $values;

	/**
	 * Constructs an aggregate of repositories.
	 *
	 * @param string $name The key to use in order to identify the current
	 * repository (or NULL to use the default).
	 */
	public function __construct(string $name = null)
	{
		$this->name = is_null($name) ? self::NAME : $name;
		$this->repositories = [];
		$this->values = [];
	}

	/**
	 * Generates a form input for distinguishing repositories.
	 *
	 * @param scalar $offset The offset on which to base the generated input
	 * (i.e. the path of the request).
	 * @param string $value The value of the generated input (i.e. the value of
	 * the possibly associated repository).
	 * @param bool $reload Whether or not the current value of the given offset
	 * should be reloaded.
	 * @return Input The generated form input.
	 */
	public function input($offset, string $value, bool $reload = false): Input
	{

		if ($reload || !array_key_exists($offset, $this->values)) {
			$this->values[$offset] = $this->value($offset);
		}

		$current = $this->values[$offset] ?? null;

		$checked = !is_null($current) && $current === $value;

		return new Input($this->name, $value, $checked);
	}

	/**
	 * Associated the given value with the given repository.
	 *
	 * @param string $value The value to associate with the given repository.
	 * @param \ArrayAccess $repository
	 */
	public function set(string $value, ArrayAccess $repository)
	{
		$this->repositories[$value] = $repository;
	}

	public function offsetExists($offset): bool
	{
		return !is_null($this->callback($offset));
	}

	public function offsetGet($offset)
	{
		return function (
			ResponseBuilderInterface $builder,
			ContextInterface $context
		) use ($offset): void {

			$callback = $this->callback($offset);

			if (is_null($callback)) {
				throw new OutOfBoundsException("No repositories contain the offset '{$offset}'");
			}

			($callback)($builder, $context);
		};
	}

	public function offsetSet($offset, $value): void
	{
		$repository = null;

		if (!($value instanceof ServerRequestInterface)) {
			throw new InvalidArgumentException('Expected value to be a PSR-compliant server request');
		}

		$id = $value->getParsedBody()[$this->name] ?? null;

		if (!is_null($id)) {
			$repository = $this->repositories["{$id}"] ?? null;
		}

		if (!is_null($repository)) {
			$this->offsetUnset($offset);
			$repository->offsetSet($offset, $value);
		}
	}

	public function offsetUnset($offset): void
	{
		foreach ($this->repositories as $repository) {

			if ($repository->offsetExists($offset)) {
				$repository->offsetUnset($offset);
			}
		}
	}

	/**
	 * Determines the callback associated with the given offset (if any).
	 *
	 * @param scalar $offset The offset to evaluate.
	 * @return callable|null The associated callback (if any).
	 */
	private function callback($offset): ?callable
	{
		$callback = null;

		foreach ($this->repositories as $repository) {

			if ($repository->offsetExists($offset)) {
				$callback = $repository->offsetGet($offset);
				break;
			}
		}

		return $callback;
	}

	/**
	 * Determines the value associated with the given offset (if any).
	 *
	 * @param scalar $offset The offset to evaluate.
	 * @return string|null The associated value (if any).
	 */
	private function value($offset): ?string
	{
		$value = null;

		foreach ($this->repositories as $value => $repository) {

			if ($repository->offsetExists($offset)) {
				break;
			}

			$value = null;
		}

		return $value;
	}
}
